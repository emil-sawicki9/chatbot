#-------------------------------------------------
#
# Project created by QtCreator 2016-12-29T13:48:37
#
#-------------------------------------------------

QT       += core gui qml quickwidgets


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chatbot
TEMPLATE = app


SOURCES += \
    src/main.cpp \
    src/ChatBot.cpp \
    src/ConceptParser.cpp \
    src/HttpClient.cpp \
    src/GraphManager.cpp \
    src/qml/QmlGraphView.cpp \
    src/graphicsView/GraphicView.cpp \
    src/graphicsView/Node.cpp \
    src/graphicsView/Edge.cpp

HEADERS  += \
    src/ChatBot.h \
    src/ConceptParser.h \
    src/HttpClient.h \
    src/GraphManager.h \
    src/qml/QmlGraphView.h \
    src/graphicsView/GraphicView.h \
    src/graphicsView/Node.h \
    src/graphicsView/Edge.h

DISTFILES += \
    src/qml/GraphView.qml \
    src/qml/Node.qml \
    src/qml/ConnectLine.qml

DEFINES += SRC_DIR=\\\"$${IN_PWD}/src/\\\"

RESOURCES += \
    qml.qrc
