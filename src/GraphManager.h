#ifndef GRAPHMANAGER_H
#define GRAPHMANAGER_H

#include <QObject>
#include <QMap>

class GraphicView;
class Node;

//struct Node;
/***********************************
 * T = (t_post - t_pre)
 * w = 2 * sigma / n + sigma
 * sigma = E 1/T
 ***********************************/
//struct Edge
//{
//  float sigma, w;
//  Node *from, *to;
//};

//struct Node
//{
//  QString word;
//  QVector<Edge*> in, out;
//  int stimulationCount, discreteTime;
//};

class GraphManager: public QObject
{
  Q_OBJECT
public:
  static GraphManager* instance();

  void runNeurons(QStringList words);

  void setGraphicView(GraphicView *view);

private:
  explicit GraphManager(QWidget *parent);

  static GraphManager* _instance;
  GraphicView *_graphView;

  QMap<QString, Node*> _map;
};

#endif // GRAPHMANAGER_H
