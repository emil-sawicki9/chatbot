var http = require('http');
var url = require('url');
var qtypes = require('qtypes');
var norm = require("node-normalizer");
var token = require("sentence-tokenizer");
//var concept = require("conceptnet");

const PORT=8080; 

function handleRequest(req, resp){
  try {
    var url_parts = url.parse(req.url, true);
    var path = url_parts.pathname;
    console.log("Request: '"+req.url+"'")
    if (req.method == 'POST') {
      req.on('data', function(data) {
        data = data.toString();
        var obj = JSON.parse(data);

        /**********************
        * Parsing sentences:
        * split => normalize => classify
        ***********************/
        if (path == "/parse")
        { 
          var tokenizer = new token("Chuck");
          tokenizer.setEntry(obj["text"]);
          // splitting
          var sentences = tokenizer.getSentences();
          var normalized = [];
          for(var i = 0; i < sentences.length ;i++)
          {
            sentences[i] = sentences[i].trim();
            // normalizing
            norm.clean(sentences[i], function(result) 
            {
              result += sentences[i].substr(-1);
              console.log(result);
              // classifing
              new qtypes(function(q) 
              {
                var sentObj = { };
                sentObj.text = result;
                sentObj.type = q.questionType(result);
                sentObj.class = q.classify(result);
                normalized.push(sentObj);
                console.log(sentObj);
                if (normalized.length == sentences.length)
                {
                  var respObj = {}
                  respObj.respType = "parse";
                  respObj.text = normalized;
                  resp.writeHead(200, {"Content-Type": "application/json"});
                  resp.end(JSON.stringify(respObj));
                }
              });
            });  
          }
        }
        /**************************
        * Getting facts from DB
        **************************/
        else if (path == "/concept")
        { 
          resp.writeHead(200, {"Content-Type": "application/json"});
          var respObj = { };
          respObj.respType = "concept";
          respObj.concept = "NULL";
          resp.end(JSON.stringify(respObj));  
        }
        /**************************
        * Classifing what 
        * type of question
        **************************/
        else if (path == "/classify")
        { 
            new qtypes(function(q) 
            {
                resp.writeHead(200, {"Content-Type": "application/json"});
                var respObj = { };
                respObj.respType = "classify";
                respObj.type = q.questionType(obj["text"]);
                respObj.class = q.classify(obj["text"]);
                resp.end(JSON.stringify(respObj));
            });
        }
        /**************************
        * Normalizing all
        * misspellings and mistakes
        **************************/
        else if (path == "/normalize")
        { 
          norm.clean(obj["text"], function(result) 
          {
            resp.writeHead(200, {"Content-Type": "application/json"});
            var respObj = { };
            respObj.respType = "normalize";
            respObj.normalized = result;
            resp.end(JSON.stringify(respObj));  
          });
        }
        /**************************
        * Splitting text into 
        * sentences
        **************************/
        else if (path == "/splitter")
        { 
          var tokenizer = new token("Chuck");
          resp.writeHead(200, {"Content-Type": "application/json"});
          tokenizer.setEntry(obj["text"]);
          var respObj = { };
          respObj.sentences = tokenizer.getSentences();
          respObj.respType = "splitter";
          resp.end(JSON.stringify(respObj));
        }
      });
      return;
    }
    else if (path == "/")
    {
        resp.writeHead(200, {"Content-Type": "application/json"});
        resp.end("It works!");
    }
  } 
  catch(err) 
  {
    resp.error(err)
    console.log(err);
  }
}

var server = http.createServer(handleRequest);

server.listen(PORT, function(){
    console.log("Loading dictionaries for Normalizer....");
    norm.loadData(function() {});
    console.log("Ended loading dictonaries\n");
    console.log("Server listening on: http://localhost:%s", PORT);
});
