import QtQuick 2.0

Item {
  property var rec_pre;
  property var rec_post;
  property var num: 0;
  property var num1: 10;
  property color genCol: generateColor();
  property color col: rec_pre.hover ? genCol : "#737373"
  property var pos: ({"x":-5, "y":-5, "x1":-5, "y1":-5});

  Component.onCompleted: pos = calcPos();

  function generateColor()
  {
    var red = Math.random()
    var green = Math.random()
    var blue = Math.random()
    return Qt.rgba(red,green,blue,1);
  }

  Connections
  {
    target: rec_pre
    onXChanged: pos = calcPos();
    onYChanged: pos = calcPos();
  }

  Connections
  {
    target: rec_post
    onXChanged: pos = calcPos();
    onYChanged: pos = calcPos();
  }

  function calcPos()
  {
    var cx = rec_pre.x + rec_pre.width/2
    var cy = rec_pre.y + rec_pre.height/2

    var c1x = rec_post.x + rec_post.width/2
    var c1y = rec_post.y + rec_post.height/2

    var diffX = Math.abs(cx - c1x);
    var diffY = Math.abs(cy - c1y);

    if (cx > c1x) // E
    {
      if (cy < c1y) // NE
      {
        if (diffX < diffY)
        {
          return getConnectPos("up");
        }
        else
        {
          return getConnectPos("right");
        }
      }
      else //SE
      {
        if (diffX < diffY)
        {
          return getConnectPos("down");
        }
        else
        {
          return getConnectPos("right");
        }
      }
    }
    else // W
    {
      if (cy > c1y)  // SW
      {
        if (diffX < diffY)
        {
          return getConnectPos("down");
        }
        else
        {
          return getConnectPos("left");
        }
      }
      else // NW
      {
        if(diffX < diffY)
        {
          return getConnectPos("up");
        }
        else
        {
          return getConnectPos("left");
        }
      }
    }
  }

  function getConnectPos(dir)
  {
    var x = -5, y = -5, x1 = -5, y1 = -5;
    if (dir == "up")
    {
      x = rec_pre.x + rec_pre.width*2/3;
      y = rec_pre.y + rec_pre.height;
      x1 = rec_post.x + rec_post.width/3;
      y1 = rec_post.y;
    }
    else if (dir == "down")
    {
      x = rec_pre.x + rec_pre.width*2/3;
      y = rec_pre.y;
      x1 = rec_post.x + rec_post.width/3
      y1 = rec_post.y + rec_post.height;
    }
    else if (dir == "left")
    {
      x = rec_pre.x + rec_pre.width;
      y = rec_pre.y + rec_pre.height/3;
      x1 = rec_post.x;
      y1 = rec_post.y + rec_post.height/3;
    }
    else if (dir == "right")
    {
      x = rec_pre.x;
      y = rec_pre.y + rec_pre.height*2/3;
      x1 = rec_post.x + rec_post.width
      y1 = rec_post.y + rec_post.height*2/3;
    }
    return {"x":x, "y":y, "x1":x1, "y1":y1};
  }

  Rectangle
  {
    id:pointer
    color: col;
    width: 7.5;
    height: width;
    radius: width/3
    x: pos.x1 - width/2;
    y: pos.y1 - height/2;
  }

}
