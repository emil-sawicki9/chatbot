import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
  id: node;
  property string word: "";
  property var edges: [];
  property var hover: ma.containsMouse || ma.drag.active || details;
  property bool details: false;
  property var discreteTime: -1;

  onHoverChanged:
  {
    node.parent.showEdgesInfo(hover ? node.edges : []);
    node.parent.repaint();
  }

  x: 50
  y: 50
  width: word == "" ? 30 : wordText.paintedWidth + 15
  height: 30
  onXChanged:
  {
    if (x+width > parent.width)
      parent.width = x+width;
  }

  onYChanged:
  {
    if (y+height > parent.height)
      parent.height = y+height;
  }

  RectangularGlow
  {
    anchors.fill: rec
    glowRadius: 5
    spread: 0.2
    color: details ? "blue" : hover ? "red" : "#262626";
    cornerRadius: rec.radius + glowRadius
  }

  Rectangle
  {
    anchors.fill: parent
    id: rec

    color: "white";

    LinearGradient
    {
      anchors.fill: parent
      start: Qt.point(0,0)
      end: Qt.point(0, rec.height);
      gradient: Gradient {
        GradientStop { position: 0.0; color: "#f2f2f2"; }
        GradientStop { position: 1.0; color: "#cccccc"; }
      }
    }

    Text
    {
      id: wordText
      anchors.centerIn: parent
      text: word;
      font.bold: hover
    }

    MouseArea
    {
      id: ma
      anchors.fill: parent
      acceptedButtons: Qt.LeftButton | Qt.RightButton
      drag
      {
        target : node;
        minimumX: 5;
        minimumY: 5;
      }
      hoverEnabled: true;
      onPositionChanged:
      {
        if (drag.active)
        {
          node.parent.repaint();
        }
      }
      onPressed:
      {
        if (pressedButtons & Qt.RightButton)
        {
//          details = !details
        }
        else
        {
          console.log("word = " + word + " | discrTime = " + discreteTime);
        }
      }
    }
  }
}
