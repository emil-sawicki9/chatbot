import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Item
{
  ScrollView
  {
    id: view

    clip: true
    anchors.fill: parent
    Canvas {
      id: graphView
      property var nodes: [];
      property var edges: [];

      renderStrategy: Canvas.Threaded
      onPaint: {
        var ctx = getContext("2d")
        for (var i = 0; i < edges.length; i++)
        {
          ctx.beginPath()
          ctx.lineWidth = edges[i].col != edges[i].genCol ? 1 : 3;
          ctx.strokeStyle = edges[i].col;
          ctx.moveTo(edges[i].pos.x, edges[i].pos.y)
          ctx.lineTo(edges[i].pos.x1, edges[i].pos.y1)
          ctx.stroke()
        }
      }

      function repaint()
      {
        var ctx = getContext("2d");
        ctx.clearRect(0,0,graphView.width,graphView.height);
        requestPaint()
      }

      function showEdgesInfo(edges)
      {
        infoRec.visible = edges.length > 0;
        edgeModel.clear()
        edgeModel.maxWidth = 40;
        for(var i = 0 ; i < edges.length; i++)
        {
          edgeModel.append({ "col":edges[i].genCol.toString(),
                             "name":edges[i].rec_post.word,
                             "num":edges[i].num,
                             "num1":edges[i].num1});
//          textMetrics.text = edges[i].rec_post.word;
//          if (textMetrics.width > edgeModel.maxWidth )
//            edgeModel.maxWidth = textMetrics.width + 5
        }
      }
    }
  }

//  TextMetrics
//  {
//    id: textMetrics
//    font.family: "Arial"
//    elide: Text.ElideMiddle
//    elideWidth: 100
//    text: ""
//  }

  Rectangle
  {
    id: infoRec
    border.width: 1
    visible: false;

    anchors.top: view.top;
    anchors.right: view.right;

    width: listView.width;
    height: listView.height;

    ListView
    {
      id: listView
      onCountChanged:
      {
        if (listView.visibleChildren.length > 0)
        {
          var root = listView.visibleChildren[0]
          var listViewHeight = 0
          var listViewWidth = 0
          for (var i = 0; i < root.visibleChildren.length; i++) {
              listViewHeight += root.visibleChildren[i].height
              listViewWidth  = Math.max(listViewWidth, root.visibleChildren[i].width)
          }

          listView.height = listViewHeight
          listView.width = listViewWidth
        }
      }

      model: edgeModel
      delegate: edgeDelegate
    }
  }

  ListModel
  {
    id: edgeModel
    property var maxWidth: 40;
  }

  Component
  {
    id: edgeDelegate
    Row
    {
      spacing: 3
      Rectangle { width: 1; height:1; color:"transparent"; }
      Rectangle
      {
        color: col;
        x: 10;
        y:text1.height/4;
        width:text1.height/2;
        height:text1.height/2;
      }
      Text { id:text1; text: name; width: edgeModel.maxWidth }
      Text { text: num; width: 40 }
      Text { text: num1; width: 40 }
    }
  }

  function connectNode(word_pre, discrTime_pre, word_post, discrTime_post)
  {
    var node_pre, node_post, edge;
    for (var i = 0 ; i < graphView.nodes.length ; i++)
    {
      if (graphView.nodes[i].word == word_pre)
      {
        node_pre = graphView.nodes[i];
        for (var j = 0 ; j < node_pre.edges.length; j++)
        {
          if (node_pre.edges[j].rec_post.word == word_post)
          {
            edge = node_pre.edges[j];
            node_post = edge.rec_post;
            break;
          }
        }
      }
      else if (graphView.nodes[i].word == word_post)
        node_post = graphView.nodes[i];

      if (typeof nodes_pre !== 'undefined' && typeof node_post !== 'undefined')
        break;
    }

    if (typeof node_pre === 'undefined')
    {
      var comp = Qt.createComponent("Node.qml");
      if (comp.status == Component.Ready)
      {
        node_pre = comp.createObject(graphView);
        node_pre.word = word_pre;
        graphView.nodes.push(node_pre);
      }
      else
        console.log("ERROR " + comp.errorString());
    }
    if (typeof node_post === 'undefined')
    {
      var comp = Qt.createComponent("Node.qml");
      if (comp.status == Component.Ready)
      {
        var go_down = placeNode(node_pre)
        node_post = comp.createObject(graphView);
        node_post.word = word_post;
        node_post.x = node_pre.x + node_pre.width * 2
        node_post.y += 40 * go_down;
        graphView.nodes.push(node_post);
        if (graphView.height < node_post.y+30)
          graphView.height = node_post.y*2;
      }
      else
        console.log("ERROR " + comp.errorString());
    }

    node_pre.discreteTime = discrTime_pre;
    node_post.discreteTime = discrTime_post;

    if (typeof edge === 'undefined')
    {
      var comp = Qt.createComponent("ConnectLine.qml");
      if (comp.status == Component.Ready)
      {
        edge = comp.createObject(graphView, {"rec_pre":node_pre, "rec_post": node_post});
        node_pre.edges.push(edge);
        graphView.edges.push(edge);
      }
    }
  }

  function placeNode(node_pre)
  {
    var go_down = 0;
    while(1)
    {
      var x_tmp = node_pre.x+node_pre.width*2;
      var y_tmp = 50 * (go_down+1);
      if (graphView.childAt(x_tmp,y_tmp) || graphView.childAt(x_tmp+2, y_tmp))
      {
        //graphView.childAt(x_tmp,y) || graphView.childAt(x_tmp+15,y) || graphView.childAt(x_tmp+30,y_tmp))
        go_down++;
      }
      else
        break;
    }
    return go_down;
  }
}
