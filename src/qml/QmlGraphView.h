#ifndef QMLGRAPHVIEW_H
#define QMLGRAPHVIEW_H

#include <QQuickWidget>
#include <QWidget>

class QmlGraphView : public QQuickWidget
{
  Q_OBJECT
public:
  static QmlGraphView* instance();

  void drawNodes(QString t1, int d1, QString t2, int d2);

private:
  explicit QmlGraphView(QWidget *p);

  static QmlGraphView *_instance;

};

#endif // QMLGRAPHVIEW_H
