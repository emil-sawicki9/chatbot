#include "QmlGraphView.h"

#include "../ChatBot.h"

#include <QQmlEngine>
#include <QQmlContext>
#include <QMetaObject>
#include <QQuickItem>

QmlGraphView* QmlGraphView::_instance = 0;
QmlGraphView::QmlGraphView(QWidget *parent)
  : QQuickWidget(parent)
{
  setResizeMode(QQuickWidget::SizeRootObjectToView);
  engine()->addImportPath(QLatin1String("modules"));
  engine()->rootContext()->setContextProperty("_graphView", this);

  QUrl qmlFile = QUrl("qrc:/src/qml/GraphView.qml");
  setSource(qmlFile);
}

QmlGraphView* QmlGraphView::instance()
{
  if (!_instance)
    _instance = new QmlGraphView(ChatBot::instance());
  return _instance;
}

void QmlGraphView::drawNodes(QString t1, int d1, QString t2, int d2)
{
  QMetaObject::invokeMethod(rootObject(), "connectNode", Qt::DirectConnection,
                            Q_ARG(QVariant, t1), Q_ARG(QVariant,d1),
                            Q_ARG(QVariant, t2), Q_ARG(QVariant,d2));
}
