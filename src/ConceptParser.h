#ifndef CONCEPTPARSER_H
#define CONCEPTPARSER_H

#include <QObject>

class ConceptParser : QObject
{
  Q_OBJECT
public:
  enum ConceptRelation
  {
    None = 0,
    RelatedTo,
    FormOf,
    IsA,
    HasProperty,
    MadeOf,
    PartOf,
    HasA,
    UsedFor,
    CapableOf,
    AtLocation,
    Causes,
    HasSubevent,
    HasPrerequisite,
    MotivatedByGoal,
    ObstructedBy,
    Desires,
    CreatedBy,
    Synonym,
    Antonym,
    DerivedFrom,
    SymbolOf,
    Entails,
    MannerOf,
    LocatedNear,
    DefinedAs,
    ReceivesAction,
  };
  Q_ENUM(ConceptRelation)

  static ConceptParser* instance();

  void parse(QJsonObject obj);

private:
  explicit ConceptParser();

  int parseRelation(QString rel);

  static ConceptParser* _instance;



};

#endif // CONCEPTPARSER_H
