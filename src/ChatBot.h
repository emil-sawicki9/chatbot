#ifndef CHATBOT_H
#define CHATBOT_H

#include <QMainWindow>
#include <QLineEdit>
#include <QTextEdit>
#include <QLabel>

#include <QDebug>

#include <QNetworkReply>
#include <QJsonObject>
#include <QMap>
#include <QVector>

class ChatBot : public QMainWindow
{
  Q_OBJECT

public:
  ChatBot(QWidget *parent = 0);
  ~ChatBot();

  static ChatBot* instance();

private slots:
  void onChatInputClicked();
  void onConceptClicked();

  void onRequestError(QNetworkReply::NetworkError error);
  void onRequestFinished(QJsonObject response);

protected:
  void keyPressEvent(QKeyEvent *event);

private:
  QStringList splitSentence(QString sent);

  static ChatBot* _instance;
  const QString _path;
  const QString _conceptPath;

  QTextEdit *_chat;
  QLineEdit *_inputText;

  QTextEdit *_edit;
  QLineEdit *_inputWord;
};

#endif // CHATBOT_H
