#ifndef NODECLIENT_H
#define NODECLIENT_H

#include <QObject>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>

class HttpClient : public QObject
{
  Q_OBJECT
public:
  static HttpClient* instance();

  void sendPostRequest(const QUrl &url, QJsonObject &obj);
  void sendGetRequest(const QUrl &url);

signals:
  void requestFinished(QJsonObject response);
  void requestError(QNetworkReply::NetworkError error);

private slots:
  void onFinished(QNetworkReply *reply);

private:
  explicit HttpClient(QObject *parent = 0);
  static HttpClient* _instance;

  QNetworkAccessManager *_manager;
};

#endif // NODECLIENT_H
