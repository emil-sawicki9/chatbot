#include "ConceptParser.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>

ConceptParser* ConceptParser::_instance = 0;
ConceptParser::ConceptParser()
{

}

ConceptParser* ConceptParser::instance()
{
  if (!_instance)
    _instance = new ConceptParser();
  return _instance;
}

void ConceptParser::parse(QJsonObject obj)
{
  QJsonArray edges = obj["edges"].toArray();
  Q_FOREACH(QJsonValue e , edges)
  {
    QJsonObject eObj = e.toObject();
    // Get relation between start and end
    ConceptRelation relation = (ConceptRelation)parseRelation(eObj["rel"].toString());
    // Get start
    QString start = eObj["start"].toString().split("/").last();
    // Get end
    QString end = eObj["end"].toString().split("/").last();

    qDebug() << start << relation << end << " | " << eObj["surfaceText"].toString().remove("[").remove("]");
  }
}

int ConceptParser::parseRelation(QString rel)
{
  int ret = None;
  if (rel.contains("RelatedTo"))
    ret = RelatedTo;
  else if (rel.contains("FormOf"))
    ret = FormOf;
  else if (rel.contains("IsA"))
    ret = IsA;
  else if (rel.contains("PartOf"))
    ret = PartOf;
  else if (rel.contains("HasA"))
    ret = HasA;
  else if (rel.contains("UsedFor"))
    ret = UsedFor;
  else if (rel.contains("CapableOf"))
    ret = CapableOf;
  else if (rel.contains("AtLocation"))
    ret = AtLocation;
  else if (rel.contains("Causes"))
    ret = Causes;
  else if (rel.contains("HasSubevent"))
    ret = HasSubevent;
  else if (rel.contains("HasPrerequisite"))
    ret = HasPrerequisite;
  else if (rel.contains("MotivatedByGoal"))
    ret = MotivatedByGoal;
  else if (rel.contains("ObstructedBy"))
    ret = ObstructedBy;
  else if (rel.contains("Desires"))
    ret = Desires;
  else if (rel.contains("CreatedBy"))
    ret = CreatedBy;
  else if (rel.contains("Synonym"))
    ret = Synonym;
  else if (rel.contains("Antonym"))
    ret = Antonym;
  else if (rel.contains("DerivedFrom"))
    ret = DerivedFrom;
  else if (rel.contains("SymbolOf"))
    ret = SymbolOf;
  else if (rel.contains("Entails"))
    ret = Entails;
  else if (rel.contains("MannerOf"))
    ret = MannerOf;
  else if (rel.contains("LocatedNear"))
    ret = LocatedNear;
  else if (rel.contains("HasProperty"))
    ret = HasProperty;
  else if (rel.contains("MadeOf"))
    ret = MadeOf;
  else if (rel.contains("DefinedAs"))
    ret = DefinedAs;
  else if (rel.contains("ReceivesAction"))
    ret = ReceivesAction;

  return ret;
}
