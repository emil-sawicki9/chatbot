#include "HttpClient.h"

#include "ChatBot.h"

#include <QUrl>

HttpClient* HttpClient::_instance = 0;
HttpClient::HttpClient(QObject *parent)
  : QObject(parent)
{
  _manager = new QNetworkAccessManager(this);
  connect(_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onFinished(QNetworkReply*)));
}

HttpClient* HttpClient::instance()
{
  if (_instance == NULL)
    _instance = new HttpClient(ChatBot::instance());
  return _instance;
}

void HttpClient::sendGetRequest(const QUrl &url)
{
  qDebug() << "GET_REQUEST: "<< url;
  QNetworkRequest request(url);
  request.setHeader(QNetworkRequest::ContentTypeHeader, QString::fromUtf8("application/json"));

  QNetworkReply *reply = _manager->get(request);

  connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(requestError(QNetworkReply::NetworkError)));
}

void HttpClient::sendPostRequest(const QUrl &url, QJsonObject &obj)
{
  qDebug() << "POST_REQUEST: "<< url << obj;
  QNetworkRequest request(url);
  request.setHeader(QNetworkRequest::ContentTypeHeader, QString::fromUtf8("application/json"));

  QNetworkReply *reply = _manager->post(request, QJsonDocument(obj).toJson());

  connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(requestError(QNetworkReply::NetworkError)));
}

void HttpClient::onFinished(QNetworkReply *reply)
{
  QByteArray arr = reply->readAll();
  QJsonObject obj = QJsonDocument::fromJson(arr).object();

  emit requestFinished(obj);
  reply->deleteLater();
}
