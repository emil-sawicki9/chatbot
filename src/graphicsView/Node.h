#ifndef NODE_H
#define NODE_H

#include <QGraphicsItem>

class Edge;

class Node : public QGraphicsItem
{
public:
  Node(const QString &word, const int discreteTime);

  QRectF boundingRect() const;
  void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);

  const QString& getWord() const;
  int getDiscreteTime() const;

  Edge* getEdge(const QString& wordFrom, const QString& wordTo);
  void addEdgeTo(Edge *e);
  void addEdgeFrom(Edge *e);

  void adjust();

protected:
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

private:
  const QString _word;
  const int _discreteTime;

  QVector<Edge*> _in, _out;
};

#endif // NODE_H
