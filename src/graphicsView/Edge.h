#ifndef EDGE_H
#define EDGE_H

#include <QGraphicsItem>

class Node;

class Edge : public QGraphicsItem
{
public:
  Edge(Node *from, Node *to);

  Node* getFromNode();
  Node* getToNode();

  QRectF boundingRect() const;
  void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
  QPainterPath shape() const;

private:
  void adjust();

  Node *_nodeFrom, *_nodeTo;
  QPointF _startPos, _endPos;
  int _arrowSize;
};

#endif // EDGE_H
