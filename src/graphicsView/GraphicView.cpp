#include "GraphicView.h"

#include <QDebug>
#include <QMouseEvent>

GraphicView::GraphicView(QWidget *parent)
  : QGraphicsView(parent)
{
  setScene(new QGraphicsScene());

  setAlignment(Qt::AlignLeft | Qt::AlignTop);

  setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
  setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

  setAcceptDrops(true);
  setMouseTracking(true);
}
