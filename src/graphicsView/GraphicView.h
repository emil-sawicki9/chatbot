#ifndef GRAPHICVIEW_H
#define GRAPHICVIEW_H

#include <QObject>
#include <QGraphicsView>

class GraphicView : public QGraphicsView
{
  Q_OBJECT
public:
  GraphicView(QWidget *parent = 0);

};

#endif // GRAPHICVIEW_H
