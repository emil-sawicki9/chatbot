#include "Edge.h"

#include <QtMath>
#include <QPen>
#include <QPainter>
#include <QDebug>
#include <math.h>

#include "Node.h"

static const double Pi = 3.14159265358979323846264338327950288419717;

Edge::Edge(Node *from, Node *to)
  : _nodeFrom(from)
  , _nodeTo(to)
  , _arrowSize(10)
{
  setZValue(-10);
  adjust();
}

QRectF Edge::boundingRect() const
{
  qreal penWidth = 1;
  qreal extra = (penWidth + _arrowSize) / 2.0;

  return QRectF(_startPos, QSizeF(_endPos.x() - _startPos.x(),
                                    _endPos.y() - _startPos.y()))
      .normalized()
      .adjusted(-extra, -extra, extra, extra);
}

QPainterPath Edge::shape() const
{
  static const qreal kClickTolerance = 10;

  QPointF vec = _endPos - _startPos;
  vec = vec*(kClickTolerance/qSqrt(QPointF::dotProduct(vec, vec)));
  QPointF orthogonal(vec.y(), -vec.x());

  QPainterPath result(_startPos - vec + orthogonal);
  result.lineTo(_startPos - vec - orthogonal);
  result.lineTo(_endPos + vec - orthogonal);
  result.lineTo(_endPos + vec + orthogonal);
  result.closeSubpath();
  return result;
}

void Edge::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  Q_UNUSED(widget);
  Q_UNUSED(option);

  adjust();

  QLineF line(_startPos, _endPos);
  if (qFuzzyCompare(line.length(), qreal(0.)))
    return;

  int dashLength = 5;
  int dashSpacing = 5;

  QPen pen(Qt::black, 1, Qt::DashLine, Qt::RoundCap, Qt::RoundJoin);
  pen.setDashPattern(QVector<qreal>() << dashLength << dashSpacing);
  painter->setPen(pen);

  // Draw the line itself
  painter->drawLine(line);

  // Draw the arrows
  double angle = ::acos(line.dx() / line.length());
  if (line.dy() >= 0)
      angle =  - angle;

  QPointF destArrowP1 = _endPos + QPointF(sin(angle - Pi / 3) * _arrowSize,
                                            cos(angle - Pi / 3) * _arrowSize);
  QPointF destArrowP2 = _endPos + QPointF(sin(angle - Pi + Pi / 3) * _arrowSize,
                                            cos(angle - Pi + Pi / 3) * _arrowSize);

  painter->setBrush(Qt::black);
  painter->drawPolygon(QPolygonF() << line.p2() << destArrowP1 << destArrowP2);

  // DEBUG SHAPE
//  painter->setBrush(Qt::NoBrush);
//  painter->setPen(QPen(QColor(Qt::red)));
//  painter->drawPath(shape());
}

Node* Edge::getFromNode()
{
  return _nodeFrom;
}

Node* Edge::getToNode()
{
  return _nodeTo;
}

void Edge::adjust()
{
	const QRectF rectFrom = _nodeFrom->boundingRect();
	const QRectF rectTo = _nodeTo->boundingRect();
	QLineF line(mapFromItem(_nodeFrom, rectFrom.width() / 2, rectFrom.height() / 2), mapFromItem(_nodeTo, rectTo.width() / 2, rectTo.height() / 2));

	prepareGeometryChange();

	qreal Ox, Oy, Dx, Dy;

	if (abs(line.dx()) < 1)
	{
		Ox = 0;
		Dx = 0;
	}
	else
	{
		Ox = line.dx() / abs(line.dx()) * std::min(int(rectFrom.width() / 2), int(fabs(rectFrom.height() / 2.0 * line.dx() / (line.dy() + 0.5))));
		Dx = -line.dx() / abs(line.dx()) * std::min(int(rectTo.width() / 2), int(fabs(rectTo.height() / 2.0 * line.dx() / (line.dy() + 0.5))));
	}
	if (abs(line.dy()) < 1)
	{
		Oy = 0;
		Dy = 0;
	}
	else
	{
		Oy = line.dy() / abs(line.dy()) * std::min(int(rectFrom.height() / 2), int(fabs(rectFrom.width() / 2.0 * line.dy() / (line.dx() + 0.5))));
		Dy = -line.dy() / abs(line.dy()) * std::min(int(rectTo.height() / 2), int(fabs(rectTo.width() / 2.0 * line.dy() / (line.dx() + 0.5))));
	}
	QPointF edgeOffsetSource(Ox, Oy);
	QPointF edgeOffsetDest(Dx, Dy);
	_startPos = line.p1() + edgeOffsetSource;
	_endPos = line.p2() + edgeOffsetDest;
}
