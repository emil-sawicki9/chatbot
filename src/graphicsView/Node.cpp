#include "Node.h"

#include <QPainter>
#include <QDebug>
#include <QGraphicsSceneDragDropEvent>
#include <QGraphicsScene>

#include "Edge.h"

Node::Node(const QString &word, const int discreteTime)
  : _word(word)
  , _discreteTime(discreteTime)
{

  this->setFlag(QGraphicsItem::ItemIsMovable, true);
}

void Node::adjust()
{
  int x = (_discreteTime - 1) * (boundingRect().width()*1.5);
  int y = 0;

  setPos(x, y);

  while(scene()->collidingItems(this).size() > 0)
  {
    y += boundingRect().height() * 1.5;
    setPos(x, y);
  }
}

const QString& Node::getWord() const
{
  return _word;
}

int Node::getDiscreteTime() const
{
  return _discreteTime;
}

QRectF Node::boundingRect() const
{
  return QRectF(0,0, 50,50);
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  Q_UNUSED(option);
  Q_UNUSED(widget);

  painter->setRenderHint(QPainter::Antialiasing);

  QPainterPath path;
  path.addRoundedRect(boundingRect(), 10, 10);

  QPen pen(Qt::black, 1);
  painter->setPen(pen);

  painter->fillPath(path, Qt::white);
  painter->drawPath(path);

  QFont font = painter->font();
  font.setBold(true);
  painter->setFont(font);

  painter->drawText(boundingRect(), Qt::AlignCenter, _word);
}

void Node::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  QGraphicsItem::mouseMoveEvent(event);

  // limiting drag to minimum (0,0) coordinates
  if (pos().y() < 0)
    setPos(pos().x(), 0);
  if (pos().x() < 0)
    setPos(0, pos().y());
}

Edge* Node::getEdge(const QString &wordFrom, const QString &wordTo)
{
  Q_FOREACH(Edge *e, _in)
  {
    if (e->getToNode()->getWord() == wordTo)
      return e;
  }

  Q_FOREACH(Edge *e, _out)
  {
    if (e->getFromNode()->getWord() == wordFrom)
      return e;
  }
  return 0;
}

void Node::addEdgeFrom(Edge *e)
{
  _out.push_back(e);
}

void Node::addEdgeTo(Edge *e)
{
  _in.push_back(e);
}
