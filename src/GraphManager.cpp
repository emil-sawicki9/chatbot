#include "GraphManager.h"

#include <QVector>
#include <QString>
#include <QMap>

#include "ChatBot.h"
#include "qml/QmlGraphView.h"
#include "graphicsView/Node.h"
#include "graphicsView/Edge.h"
#include "graphicsView/GraphicView.h"

GraphManager* GraphManager::_instance = 0;
GraphManager::GraphManager(QWidget *parent)
  : QObject(parent)
  , _graphView(0)
{

}

GraphManager* GraphManager::instance()
{
  if (!_instance)
    _instance = new GraphManager(ChatBot::instance());

  return _instance;
}

void GraphManager::setGraphicView(GraphicView *view)
{
  _graphView = view;
}

void GraphManager::runNeurons(QStringList words)
{
  if (!_graphView)
    return;

  for(int i = 0 ; i < words.length() - 1; i++)
  {
    const QString &wordFrom = words[i];
    const QString &wordTo = words[i+1];
    Node *nodeFrom = 0, *nodeTo = 0;

    if (_map.contains(wordFrom))
    {
      nodeFrom = _map[wordFrom];
//      from->stimulationCount++;
    }
    else
    {
      nodeFrom = new Node(wordFrom, 1);
      _graphView->scene()->addItem(nodeFrom);
      nodeFrom->adjust();
      _map.insert(nodeFrom->getWord(), nodeFrom);
    }

    if (_map.contains(wordTo))
      nodeTo = _map[wordTo];
    else
    {
      nodeTo = new Node(wordTo, nodeFrom->getDiscreteTime() + 1);
      _graphView->scene()->addItem(nodeTo);
      nodeTo->adjust();
      _map.insert(nodeTo->getWord(), nodeTo);
    }

    Edge *eFrom, *eTo;
    Edge *edge = 0;

    eFrom = nodeFrom->getEdge(wordFrom, wordTo);
    eTo = nodeTo->getEdge(wordFrom, wordTo);

    if (eFrom && !eTo)
    {
      edge = eFrom;
      nodeTo->addEdgeTo(edge);
    }
    else if (!eFrom && eTo)
    {
      edge = eTo;
      nodeFrom->addEdgeFrom(edge);
    }

    if (edge == 0)
    {
      edge = new Edge(nodeFrom, nodeTo);
      _graphView->scene()->addItem(edge);
    }


  }
}
