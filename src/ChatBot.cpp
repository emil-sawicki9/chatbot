#include "ChatBot.h"

#include <QHBoxLayout>
#include <QPushButton>
#include <QTabWidget>
#include <QJsonArray>

#include <QMessageBox>
#include <QDockWidget>
#include <QKeyEvent>
#include <QApplication>

#include "HttpClient.h"
#include "ConceptParser.h"
#include "GraphManager.h"

#include "graphicsView/GraphicView.h"

ChatBot* ChatBot::_instance = 0;
ChatBot::ChatBot(QWidget *parent)
  : QMainWindow(parent)
  , _path(QStringLiteral("http://localhost:8080"))
  , _conceptPath(QStringLiteral("http://conceptnet5.media.mit.edu/data/5.4/"))
{
  _instance = this;

  this->setWindowTitle("AAN ChatBot");
  this->resize(1024, 768);

  connect(HttpClient::instance(), SIGNAL(requestFinished(QJsonObject)), this, SLOT(onRequestFinished(QJsonObject)));
  connect(HttpClient::instance(), SIGNAL(requestError(QNetworkReply::NetworkError)), this, SLOT(onRequestError(QNetworkReply::NetworkError)));


  QDockWidget *chatDock = new QDockWidget("Chat");
  chatDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
  {
    QWidget *w = new QWidget(chatDock);
    QVBoxLayout *lay = new QVBoxLayout(w);
    _chat = new QTextEdit();
    _chat->setReadOnly(true);
    lay->addWidget(_chat);

    QHBoxLayout *inputLay = new QHBoxLayout();
    _inputText = new QLineEdit();
    connect(_inputText, SIGNAL(returnPressed()), this, SLOT(onChatInputClicked()));
    inputLay->addWidget(_inputText);
    QPushButton *inputBut = new QPushButton(QString::fromUtf8("OK"));
    connect(inputBut, SIGNAL(clicked(bool)), this, SLOT(onChatInputClicked()));
    inputLay->addWidget(inputBut);

    lay->addLayout(inputLay);
    chatDock->setWidget(w);
  }
  this->addDockWidget(Qt::RightDockWidgetArea, chatDock);

  QDockWidget *graphDock = new QDockWidget("LANAKG");
  graphDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
  GraphicView *graphView = new GraphicView(this);
  GraphManager::instance()->setGraphicView(graphView);
  graphDock->setWidget(graphView);
//  this->addDockWidget(Qt::LeftDockWidgetArea, graphDock);
  this->tabifyDockWidget(chatDock, graphDock);

  QDockWidget *conceptDock = new QDockWidget("ConceptNet");
  conceptDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
  {
    QWidget *w = new QWidget(conceptDock);
    QVBoxLayout *lay = new QVBoxLayout(w);
    _edit = new QTextEdit();
    lay->addWidget(_edit);
    _inputWord = new QLineEdit();
    connect(_inputWord, SIGNAL(returnPressed()), this, SLOT(onConceptClicked()));
    lay->addWidget(_inputWord);
    conceptDock->setWidget(w);
  }
  this->tabifyDockWidget(chatDock, conceptDock);

//  chatDock->raise();
  graphDock->raise();

  QString text = "A computer is a device that can be instructed to carry out an "
                 "arbitrary set of arithmetic or logical operations automatically. "
                 "The ability of computers to follow a sequence of operations, "
                 "called a program, make computers very applicable to a wide range "
                 "of tasks. Such computers are used as control systems for a very wide "
                 "variety of industrial and consumer devices. This includes simple "
                 "special purpose devices like microwave ovens and remote controls, "
                 "factory devices such as industrial robots and computer assisted design, "
                 "but also in general purpose devices like personal computers and mobile "
                 "devices such as smartphones. The Internet is run on computers and it "
                 "connects millions of other computers.";
      ;
  QRegExp rx("(\\!|\\.|\\?)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
  QStringList query = text.split(rx);
  Q_FOREACH(QString sent, query)
  {
    GraphManager::instance()->runNeurons(splitSentence(sent));
  }

//  QString text = "A1 A2";
//  GraphManager::instance()->runNeurons(splitSentence(text));
//  text = "A1 A4.";
//  GraphManager::instance()->runNeurons(splitSentence(text));
//  text = "A1 A5";
//  GraphManager::instance()->runNeurons(splitSentence(text));
//  text = "A3 A1.";
//  GraphManager::instance()->runNeurons(splitSentence(text));

  graphView->scroll(0,0);
}

ChatBot::~ChatBot()
{
}

void ChatBot::keyPressEvent(QKeyEvent *event)
{
  if (event->key() == Qt::Key_Pause)
    QApplication::exit(0);
}

void ChatBot::onConceptClicked()
{
  // DEBUG - limit for testing
  QString url = _conceptPath + "c/en/"+_inputWord->text() + "?limit=20";
  HttpClient::instance()->sendGetRequest(url);
}

void ChatBot::onChatInputClicked()
{
  QString text = _inputText->text();
  _inputText->clear();
  _chat->append(QString::fromUtf8("ME > ") + text);

  GraphManager::instance()->runNeurons(splitSentence(text));

//  QUrl url(_path + QString::fromUtf8("/parse"));
//  QJsonObject obj;
//  obj.insert(QString::fromUtf8("text"), text);
//  HttpClient::instance()->sendPostRequest(url, obj);
}

void ChatBot::onRequestError(QNetworkReply::NetworkError error)
{
  QString errorText = QString();
  QDebug(&errorText) << error;
  QMessageBox::warning(0, QString::fromUtf8("Error"), errorText);
}

void ChatBot::onRequestFinished(QJsonObject response)
{
  /**************************************************
   * Request order:
   * PARSE (split => normalize => classify) => CONCEPT
   ***************************************************/
  QString type = response[QString::fromUtf8("respType")].toString();
  if(type.isEmpty())
  {
    response["respType"] = "concept";
    type= "concept";
    qDebug() << "\nRESPONSE:" << type;
  }
  else
    qDebug() << "\nRESPONSE:" << type << "=>" << response << "\n";

  if (type == QString::fromUtf8("concept"))
  {
    if (response.contains("error"))
    {
      QMessageBox::warning(0, "ConceptNet Error", response["error"].toString());
    }
    else if (response["numFound"] == "0")
    {
      qDebug() << "RESULT NOT FOUND - CONCEPTNET";
    }
    else
    {
      ConceptParser::instance()->parse(response);
    }
  }
  else if (type == QString::fromUtf8("parse"))
  {    
    //adding word to map
    QJsonArray sentences = response["text"].toArray();
    Q_FOREACH(QJsonValue s , sentences)
    {
      QJsonObject sentObj = s.toObject();
      QStringList words = splitSentence(sentObj["text"].toString().trimmed());
      GraphManager::instance()->runNeurons(words);
    }
  }
}

QStringList ChatBot::splitSentence(QString sent)
{
  QString endMark = "";
  sent = sent.toLower();
  if (sent.endsWith('.') || sent.endsWith('?') || sent.endsWith('!'))
  {
    endMark = sent[sent.length() - 1];
    sent.chop(1);
  }
  sent = sent.remove(".").remove(",").remove(";");
  QStringList words = sent.split(' ');
  Q_FOREACH(QString w, words)
  {
    if (w.trimmed().isEmpty())
      words.removeAll(w);
  }

  if (!endMark.isEmpty())
    words.append(endMark);
  return words;
}

ChatBot* ChatBot::instance()
{
  return _instance;
}
